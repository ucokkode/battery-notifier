#[cfg(not(native))]
mod default_impl;


#[cfg(not(feature="native"))]
pub type UtilityImpl = default_impl::UtilityDefaultImpl; 

#[cfg(feature="native")]
mod native_impl;

#[cfg(feature="native")]
pub type UtilityImpl = native_impl::UtilityNativeImpl; 
