use std::{env, time::Duration};

use anyhow::bail;
use notify_rust::Notification;
use sysinfo::ProcessExt;
use sysinfo::SystemExt;
use traits::{BatteryState, Runtime, Utility};

mod get_battery;
mod runtime;
mod traits;
mod utility;

pub use get_battery::UtilityImpl;
pub use runtime::run;

fn is_process_running(process_name: &str) -> anyhow::Result<bool> {
    let s = sysinfo::System::new_all();

    let output: Vec<&sysinfo::Process> = s.processes_by_name(process_name).collect::<_>();
    let Ok(this_id) = sysinfo::get_current_pid() else {
        bail!("failed to get pid");
    };
    let pgrep: Vec<_> = output.into_iter().filter(|p| p.pid() != this_id).collect();
    Ok(!pgrep.is_empty())
}

async fn ring_the_bell<R: Runtime>(
    notification: &mut Notification,
    battery: &str,
    ring_path: String,
) -> anyhow::Result<()> {
    utility::notification_handler(notification, &format!("battery:  {battery}"))?;
    let _ = <R as Runtime>::spawn(async move {
        if let Err(err) = utility::ring_the_alarm(ring_path) {
            eprintln!("{err:?}");
        }
    })
    .await;
    Ok(())
}

#[inline(always)]
async fn try_main<R, U>(notification: &mut Notification) -> anyhow::Result<()>
where
    R: Runtime,
    U: Utility,
{
    <R as Runtime>::sleep(Duration::from_millis(500)).await;
    // get path alarn ringtone
    let args: Vec<_> = env::args().skip(1).take(2).collect();

    if args.len() < 2 {
        bail!("needs 2 argument for battery full and low");
    }

    let notification = notification.summary("battery notifier").icon("battery");
    utility::notification_handler(notification, "running in background")?;

    if is_process_running("battery-noti")? {
        utility::notification_handler(notification, "app already running")?;
        return Ok(());
    }

    loop {
        <R as Runtime>::sleep(Duration::from_secs(1)).await;
        let battery_full_ringtone = args[0].clone();
        let battery_low_ringtone = args[1].clone();

        let state = <U as Utility>::state();

        #[cfg(debug_assertions)]
        println!("on loop: {:?}", <U as Utility>::state());

        if state == BatteryState::Normal {
            continue;
        }

        if state == BatteryState::Unknown {
            utility::notification_handler(notification, "battery:  Unknown")?;
            <R as Runtime>::sleep(Duration::from_secs(3)).await;
        }

        if state == BatteryState::Full {
            ring_the_bell::<R>(notification, "Full", battery_full_ringtone).await?;
        }
        if state == BatteryState::Low {
            ring_the_bell::<R>(notification, "Low", battery_low_ringtone).await?;
        }
    }
}
