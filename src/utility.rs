use std::io::BufReader;

use anyhow::Context;
use notify_rust::Notification;

pub(super) fn ring_the_alarm(path: String) -> anyhow::Result<()> {
    let (_stream, handle) = rodio::OutputStream::try_default()?;
    let sink = rodio::Sink::try_new(&handle)?;
    let file = std::fs::File::open(path)?;
    sink.append(rodio::Decoder::new(BufReader::new(file))?);
    sink.sleep_until_end();
    Ok(())
}

pub fn notification_handler(notification: &mut Notification, msg: &str) -> anyhow::Result<()> {
    notification
        .summary("battery notifier")
        .body(msg)
        .icon("battery")
        .show()
        .map(|_| ())
        .context("failed to show notification")
}
