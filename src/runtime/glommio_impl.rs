pub use glommio::LocalExecutor;
use glommio::{timer::Timer, LocalExecutorBuilder};
use notify_rust::Notification;

use crate::{try_main, utility::notification_handler, UtilityImpl};

struct GlommioImpl;

impl crate::traits::Runtime for GlommioImpl {
    type S = Timer;
    type Task<Output> = glommio::Task<Output>;

    fn sleep(duration: std::time::Duration) -> Self::S {
        Timer::new(duration)
    }

    fn spawn<F: std::future::Future>(func: F) -> Self::Task<F::Output>
    where
        F: Send + 'static,
        F::Output: Send + 'static,
    {
        glommio::spawn_local(func)
    }
}

pub fn main(notification: &mut Notification) {
    let ex = LocalExecutorBuilder::default();
    let _ = ex
        .spawn(|| async move {
            if let Err(e) = try_main::<GlommioImpl, UtilityImpl>(notification).await {
                let _ = notification_handler(notification, &format!("Error on \n{e}"));
            };
        })
        .unwrap()
        .join();
}
