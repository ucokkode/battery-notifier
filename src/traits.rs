use std::{future::Future, time::Duration};

pub trait Runtime {
    type S: Future;
    type Task<Output>: Future;
    fn sleep(duration: Duration) -> Self::S;
    fn spawn<F: Future>(func: F) -> Self::Task<F::Output> 
    where
        F: Send + 'static,
        F::Output: Send + 'static
    ;
}

#[derive(Debug, PartialEq, Eq)]
pub enum BatteryState {
    Full,
    Normal,
    Low,
    Unknown
}

pub trait Utility {
    fn state() -> BatteryState;
}
