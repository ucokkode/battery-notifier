use crate::traits::{BatteryState, Utility};

pub struct UtilityNativeImpl;

impl Utility for UtilityNativeImpl {
    fn state() -> BatteryState {
        let Ok(manager) = battery::Manager::new() else {
            return BatteryState::Normal;
        };
        let Ok(mut batteries) = manager.batteries() else {
            return BatteryState::Normal;
        };
        let Some(Ok(battery)) = batteries.next() else {
            return BatteryState::Normal;
        };
        let ratio: f32 = battery.state_of_charge().into();
        let state = battery.state();

        #[cfg(debug_assertions)]
        println!("using native state is {:?}", state);

        match state {
            battery::State::Discharging if ratio < 0.15 => BatteryState::Low,
            battery::State::Charging | battery::State::Full if ratio >= 0.85 => BatteryState::Full,
            battery::State::Unknown => BatteryState::Unknown,
            _ => BatteryState::Normal,
        }
    }
}
