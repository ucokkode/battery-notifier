use notify_rust::Notification;
use tokio::{task::JoinHandle, time::Sleep};

use crate::{try_main, utility::notification_handler, UtilityImpl};

struct TokioImpl;

impl crate::traits::Runtime for TokioImpl {
    type S = Sleep;

    type Task<Output> = JoinHandle<Output>;

    fn sleep(duration: std::time::Duration) -> Self::S {
        tokio::time::sleep(duration)
    }

    fn spawn<F: std::future::Future>(func: F) -> Self::Task<F::Output>
    where
        F: Send + 'static,
        F::Output: Send + 'static,
    {
        tokio::spawn(func)
    }
}

pub fn main(notification: &mut Notification) {
    let runtime = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .expect("Failed building the Runtime");
    runtime.block_on(async move {
        if let Err(e) = try_main::<TokioImpl, UtilityImpl>(notification).await {
            let _ = notification_handler(notification, &format!("Error on \n{e}"));
        };
    })
}
