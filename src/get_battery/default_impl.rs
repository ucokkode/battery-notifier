use std::{process::Command, io::BufRead};

use anyhow::bail;

use crate::traits::{Utility, BatteryState};

pub struct UtilityDefaultImpl;

impl Utility for UtilityDefaultImpl {
    fn state() -> BatteryState {
        let Ok((state, percentage)) = get_battery() else {
            return BatteryState::Normal;
        };
        #[cfg(debug_assertions)]
        println!("using upower state is {:?}", state);

        match state.as_ref() {
            "discharging" if percentage < 15 => BatteryState::Low,
            "charging" | "fully-charged" if percentage >= 85  => BatteryState::Full,
            "pending-charge" => BatteryState::Unknown,
            _ => BatteryState::Normal
        } 
    }
}

#[inline(always)]
fn filter_value<E>(content: &Result<String, E>) -> bool {
    content.as_ref().map_or(false, |inner| {
        inner.contains("percentage") || inner.contains("state")
    })
}

#[inline(always)]
fn split_get_value(content: String) -> String {
    let temp = content.split_whitespace().collect::<Vec<&str>>();
    temp[1].to_string()
}



fn get_battery() -> anyhow::Result<(String, i32)> {
    let upower = Command::new("upower")
        .args(["-i", "/org/freedesktop/UPower/devices/battery_BAT0"])
        .output()?;
    let grep = upower
        .stdout
        .lines()
        .filter(filter_value)
        .filter_map(Result::ok)
        .map(split_get_value)
        .collect::<Vec<_>>();
    if grep.is_empty() {
        bail!("didn't get anough value of percentage or state of charging")
    }
    Ok((
        grep[0].to_string(),
        grep[1].replace('%', "").parse().unwrap(),
    ))
}
