### Battery Notifier
This program is designed to monitor  laptop's battery level and send notifications when it's fully charged.
perhaps only works in <em>Pop!_OS<em>


before install you need: 
    - cargo

to install run command below : 
```
git clone https://gitlab.com/ucokkode/battery-notifier.git \
cd battery-notifier 
```
```
cargo install --path . 
```
or 
```
cargo install --path . --features glommio
```

