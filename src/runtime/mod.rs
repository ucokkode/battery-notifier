
#[cfg(not(feature="glommio"))]
mod tokio_impl;

use notify_rust::Notification;
#[cfg(not(feature="glommio"))]
use tokio_impl::main;


#[cfg(feature="glommio")]
mod glommio_impl;

#[cfg(feature="glommio")]
use glommio_impl::main;

pub fn run() {
    let mut notification = Notification::new();
    main(&mut notification);
}


